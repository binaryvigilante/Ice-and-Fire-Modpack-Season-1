# Ice and Fire Modpack Season 1
 My modpack for Minecraft 1.12.2 for my YouTube series Ice and Fire Season 1!

**I am no longer maintaining this project as I'm no longer continuing this series as we have moved on to season 2.**

## Installation
1. Download the modpack from my website (https://binaryvigilante.com/downloads/ice-and-fire-season-1-modpack/)
2. Install Forge for Minecraft 1.12.2
3. Extract the `mods` folder and the `configs` folder to your `.minecraft` folder
4. Done :)
